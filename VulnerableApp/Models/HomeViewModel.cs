﻿namespace VulnerableApp.Models
{
    public class HomeViewModel
    {
        public string SensitiveData { get; set; }
    }
}