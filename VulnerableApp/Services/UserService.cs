﻿using VulnerableApp.Repositories;

namespace VulnerableApp.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _repository;

        public UserService(IUserRepository repository)
        {
            _repository = repository;
        }
        public string GetNonSensitiveDataById(string id)
        {
            return _repository.GetNonSensitiveDataById(id);
        }
    }

    public interface IUserService
    {
        public string GetNonSensitiveDataById(string id);
    }
}